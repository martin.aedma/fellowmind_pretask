using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<FellowmindDb>(options =>
    options.UseNpgsql(connectionString));

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapPost("/participants/", async(Participant n, FellowmindDb db)=> {
    db.Participants.Add(n);
    await db.SaveChangesAsync();

    return Results.Created($"/participants/{n.id}", n);
});

app.MapGet("/participants/", (FellowmindDb db) =>
{
    return db.Participants.FromSqlRaw<Participant>("Select * from \"Participants\"").ToList<Participant>();
});


app.MapPost("/meetings/", async(Meeting n, FellowmindDb db)=> {
    db.Meetings.Add(n);
    await db.SaveChangesAsync();

    return Results.Created($"/meetings/{n.id}", n);
});

app.MapGet("/meetings/", (FellowmindDb db) =>
{
    return db.Meetings.FromSqlRaw<Meeting>("Select * from \"Meetings\"").ToList<Meeting>();
});

app.MapGet("/addtomeeting/", (FellowmindDb db)=> {

    return db.Meetings.FromSqlRaw<Meeting>(
        "SELECT id, subject, organizer, starttime, endtime, tmp.participants " +
        "from \"Meetings\" m " +
                 "left join lateral (" +
            "select jsonb_agg(row_to_json(tp)) as participants " +
            "from (select p.name, p.title " +
                  "from \"Meetings_Participants\" mp " +
                           "inner join \"Participants\" p on mp.\"ParticipantId\" = p.id " +
                  "where mp.\"MeetingId\" = m.id " +
                 ") tp" +
            ") tmp on true " +
        "WHERE starttime::date <= NOW()::date "+
          "and endtime::date >= NOW()::date"
        ).ToList<Meeting>();
});


await app.RunAsync();



record Participant(int id){
    public string name {get;set;} = default!;
    public string title {get;set;} = default!;

    // Navigation Properties
    public List<Meeting_Participant> Meeting_Participants {get;set;} = default!;

}

record Meeting(int id){
    public string subject {get; set;} = default!;
    public string organizer {get; set;} = default!;
    public DateTime starttime {get; set;} = default!;
    public DateTime endtime {get; set;} = default!;

    // Navigation Properties
    public List<Meeting_Participant> Meeting_Participants {get;set;} = default!;
}

record Meeting_Participant(int id){
    public int MeetingId { get; set;}
    public Meeting Meeting {get; set;} = default!;
    public int ParticipantId { get; set;}
    public Participant Participant {get; set;} = default!;
}


class FellowmindDb: DbContext {
    public FellowmindDb(DbContextOptions<FellowmindDb> options): base(options) {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Meeting_Participant>()
            .HasOne(p => p.Participant)
            .WithMany(pa => pa.Meeting_Participants)
            .HasForeignKey(pi => pi.ParticipantId);

        modelBuilder.Entity<Meeting_Participant>()
            .HasOne(p => p.Meeting)
            .WithMany(pa => pa.Meeting_Participants)
            .HasForeignKey(pi => pi.MeetingId);
    }

    public DbSet<Participant> Participants => Set<Participant>();
    public DbSet<Meeting> Meetings => Set<Meeting>();
    public DbSet<Meeting_Participant> Meetings_Participants { get; set;} = default!;
}