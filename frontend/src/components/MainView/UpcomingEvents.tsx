import { useEffect, useState, useRef } from "react"
import { getHoursAndMinutes } from "../../utilities/functions"
import { Meeting } from "../../@types/custom"
import { useResponsiveBreakpoints, findBreakPoint } from '../../utilities/functions'

interface UpcomingEventsProps {
    meetings: Meeting[]
}
interface Breakpoints {
    one: number;
    two: number;
}

export const UpcomingEvents = ({meetings}: UpcomingEventsProps):JSX.Element => {

    const [filteredEvents, setFilteredEvents] = useState<Meeting[]>([])
    const currentTime = new Date().getTime()
    const containerRef = useRef<HTMLDivElement>(null)
    const breakPoints: Breakpoints = {
        one: 400,
        two: 580
    }
    const sizes = useResponsiveBreakpoints(containerRef)

    useEffect(() => {
      if(sizes?.width){
        setFilteredEvents(filterEvents(meetings, currentTime, findBreakPoint(breakPoints, sizes.width)))
      }
    }, [sizes])

    const filterEvents = (meetings: Meeting[], currentTime: number, size: number ):Meeting[] => {
      const filtered: Meeting[] = []

      for (const meeting of meetings) {
        if(filtered.length === size){
          return filtered
        } else {
          if (new Date(meeting.StartTime).getTime() > currentTime){
            filtered.push(meeting)
          }
        }
      }
      return filtered
    }

    return <div className="mainview-content-bottom-container" ref={containerRef}>
        {filteredEvents.map((event, index) => {
            if (new Date(event.StartTime).getTime() > currentTime){
                return <div key={index} className="upcoming-event">
                    <span>{`${getHoursAndMinutes(new Date(event.StartTime))}-${getHoursAndMinutes(new Date(event.EndTime))}`}</span>
                    <span>{event.Subject}</span>
                    <span>{event.Organizer.toUpperCase()}</span>
                </div>
            }
            return <></>
        })}
    </div>
}

