import { useEffect, useState } from 'react'
import { Meeting } from '../../@types/custom'
import { getHoursAndMinutes} from '../../utilities/functions'

interface MeetingHeaderProps {
    currentMeeting: Meeting | false;
}

interface TimerProps {
    timerInfo: {start: string, end: string}
}

export const MeetingHeader = ({currentMeeting}: MeetingHeaderProps):JSX.Element => {


    const [time, setTime] = useState(getHoursAndMinutes(new Date()))

    useEffect(() => {
        const interval = setInterval(() => {
            setTime(prev => getHoursAndMinutes(new Date()))
          }, 10000);
          return () => clearInterval(interval);
    }, [])

    const headerData = {
        current: currentMeeting ? 'CURRENT MEETING' : '',
        subject: currentMeeting ? currentMeeting.Subject.toUpperCase() : time ,
        start: currentMeeting ? getHoursAndMinutes(new Date(currentMeeting.StartTime)) : '',
        end: currentMeeting ? getHoursAndMinutes(new Date(currentMeeting.EndTime)) : '',
        organizer: currentMeeting ? currentMeeting.Organizer.toUpperCase() : ''
    }

    return <div className="mainview-content-top-container">
        <span className="current-meeting">{headerData.current}</span>
        <hr className="headline-separator"/>
        <span className="meeting-headline">{headerData.subject}</span>
        {currentMeeting && <>
        <div className='currentmeeting-timer-info current-meeting'>
            <span>{headerData.start}</span>
            <Timer timerInfo={{start: currentMeeting.StartTime, end: currentMeeting.EndTime}}/>
            <span>{headerData.end}</span>
        </div>
        <span className='current-meeting meeting-organizer'>{headerData.organizer}</span>
        </>}
    </div>
}

const Timer = ({timerInfo}: TimerProps):JSX.Element => {

    const [progressPercentage, setProgressPercetange] = useState<number>(0)

    useEffect(() => {
        const interval = setInterval(() => {
            setProgressPercetange(calculateProgress)
          }, 1000);
          return () => clearInterval(interval);
    }, [])

    const calculateProgress = ():number => {
        const eventStart = new Date(timerInfo.start).getTime()
        const eventEnd = new Date(timerInfo.end).getTime()
        const eventLength = eventEnd - eventStart
        const currentEventProgress = eventEnd - new Date().getTime()
        return 100 - (currentEventProgress * 100) / eventLength
    }

    return <div className="timer-container">
        <div className="timer-bar" style={{width: `${progressPercentage}%`}}></div>
    </div>
}