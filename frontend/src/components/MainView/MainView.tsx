import { MeetingHeader } from "./MeetingHeader"
import { UpcomingEvents } from "./UpcomingEvents"
import { Meeting } from '../../@types/custom'

interface MainViewProps {
    currentMeeting: Meeting | false;
    meetings: Meeting[];
    showMobile: boolean;
    isViewingMain: boolean;
}

export const MainView = ({currentMeeting, meetings, showMobile, isViewingMain}: MainViewProps):JSX.Element => {

    return <div className={`mainview-container ${showMobile ? isViewingMain ? 'flex-open' : 'flex-close' : ''}`}>
        <MeetingHeader currentMeeting = {currentMeeting} />
        <UpcomingEvents meetings={meetings} />
    </div>
}