import { useState, useEffect } from "react"
import { Meeting } from "../../@types/custom"

interface SpotterProps {
    refElementData: RefElementData;
    scheduleStart: number;
    scheduleLength: number;
}

interface MeetingBoxProps extends SpotterProps {
    meeting: Meeting;
    setIsViewingEvent: (value: ViewingEvent) => void;
}

interface BoxOffsets {
    offsetY: number;
    boxHeigth: number;
    boxWidth: number;
}

interface RefElementData {
    scheduleContainerHeight: number;
    timelineContainerHeigth: number;
    timelineContainerWidth: number;
}

interface ViewingEvent {
    isViewing: boolean;
    meeting: Meeting
}


export const MeetingBox = ({setIsViewingEvent, refElementData, scheduleStart, scheduleLength, meeting}: MeetingBoxProps):JSX.Element => {

    const [boxOffsets, setBoxOffsets] = useState<BoxOffsets>({offsetY: 0, boxHeigth: 0, boxWidth: 0})

    useEffect(() => {
        setBoxOffsets(calculateBoxOffsets(refElementData.timelineContainerHeigth, refElementData.scheduleContainerHeight))
    }, [refElementData])

    const calculateBoxOffsets = (modification: number, containerHeigth: number):BoxOffsets => {
        // calculate Y offset
        const startDate = new Date(meeting.StartTime)
        const startTime = startDate.getHours() + startDate.getMinutes() / 60
        const normalized = startTime - scheduleStart
        const offsetPercentage = normalized / scheduleLength

        // calculate heigth
        const endDate = new Date(meeting.EndTime)
        const meetingTime = (endDate.getHours() + endDate.getMinutes() / 60)
        const normalizedHeigth = meetingTime - startTime
        const heightPercentage = normalizedHeigth / scheduleLength

        return {offsetY: containerHeigth * offsetPercentage - (normalized * (modification / 2) + normalized - normalized / 100), boxHeigth: containerHeigth * heightPercentage - normalizedHeigth * modification / 2, boxWidth: refElementData.timelineContainerWidth}
    }

    const handleClick = () => {
        return (event: React.MouseEvent) => {
            setIsViewingEvent({isViewing: true, meeting: meeting})
        }
    }

    return <div
        className="sidebar-meetingbox-container"
        style={{transform: `translateY(${boxOffsets.offsetY}px) translateX(47px)`, height:`${boxOffsets.boxHeigth}px`}}
        onClick={handleClick()}>
            <span className="sidebar-meetingbox-subject">{meeting.Subject}</span>
            <span className="sidebar-meetingbox-organizer">{meeting.Organizer}</span>
    </div>
}