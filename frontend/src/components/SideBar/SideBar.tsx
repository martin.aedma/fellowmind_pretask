import { useState } from "react"
import { Meeting } from "../../@types/custom"
import { EventView } from "./EventView"
import { Schedule } from "./Schedule"

interface SideBarProps {
    meetings: Meeting[];
    showMobile: boolean;
    isViewingMain: boolean;
}

interface ViewingEvent {
    isViewing: boolean;
    meeting: Meeting
}

export const SideBar = ({meetings, showMobile, isViewingMain}:SideBarProps):JSX.Element => {

    const [isViewingEvent, setIsViewingEvent] = useState<ViewingEvent>({isViewing: false, meeting: meetings[0]})

    return <div className={`sidebar-container ${showMobile ? isViewingMain ? 'flex-close' : 'flex-open' : ''}`}>
        {!isViewingEvent.isViewing && <Schedule setIsViewingEvent={setIsViewingEvent} meetings={meetings}/>}
        {isViewingEvent.isViewing && isViewingEvent.meeting && <EventView isViewingEvent={isViewingEvent} setIsViewingEvent={setIsViewingEvent} />}
    </div>
}