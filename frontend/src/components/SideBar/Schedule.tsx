import { useEffect, useRef, useState } from "react"
import { Meeting } from "../../@types/custom"
import { Spotter } from './Spotter'
import { MeetingBox } from './MeetingBox'
import { useResponsiveBreakpoints } from "../../utilities/functions"

interface ScheduleProps {
    meetings: Meeting[];
    setIsViewingEvent: (value: ViewingEvent) => void;
}
interface RefElementData {
    scheduleContainerHeight: number;
    timelineContainerHeigth: number;
    timelineContainerWidth: number;
}

interface ViewingEvent {
    isViewing: boolean;
    meeting: Meeting
}

export const Schedule = ({meetings, setIsViewingEvent}: ScheduleProps):JSX.Element => {
    const scheduleStart = 7
    const scheduleEnd = 22
    const scheduleLength = scheduleEnd - scheduleStart
    const containerRef = useRef<HTMLDivElement>(null)
    const timelineRef = useRef<HTMLDivElement>(null)
    const overlayRef = useRef<HTMLDivElement>(null)
    const [refData, setRefData] = useState<RefElementData>({scheduleContainerHeight: 0, timelineContainerHeigth: 0, timelineContainerWidth: 0})
    const [heigthOffset, setHeigtOffset] = useState(0)
    const sizes = useResponsiveBreakpoints(overlayRef)

    useEffect(() => {
        if(overlayRef.current){
            setHeigtOffset(overlayRef.current.clientHeight)
        }
    }, [sizes])

    useEffect(() => {
        if(timelineRef.current && containerRef.current){
            setRefData({
                scheduleContainerHeight: containerRef.current.scrollHeight,
                timelineContainerHeigth: timelineRef.current.clientHeight,
                timelineContainerWidth: timelineRef.current.clientWidth})
        }
    }, [timelineRef.current, containerRef.current])

    const createScheduleTimes = ():string[] => {
        const scheduleTimes:string[] = []
        for(let i = scheduleStart; i < scheduleEnd+1; i++){
            scheduleTimes.push(`${i}:00`)
            scheduleTimes.push(`${i}:30`)
        }
        return scheduleTimes
    }

    return  <><div className="sidebar-header-container" ref={overlayRef}>
        <span className="sidebar-conference-room">CONFERENCE ROOM</span>
        <span className="sidebar-conference-room-name">KESÄ TODAY</span>
        </div>
    <div className="sidebar-schedule-overlay" style={{top: heigthOffset - 3}}></div>

    <div className="sidebar-schedule-container" ref={containerRef}>
        {createScheduleTimes().map((time, index) => {
            return <div key={index} className="sidebar-timeline-container" ref={timelineRef}>
                <span className="sidebar-timeline-time">{time}</span><hr className="sidebar-time-line"/>
            </div>
        })}
        {meetings.map((meeting, index) => {
            return <MeetingBox
                setIsViewingEvent={setIsViewingEvent}
                key={index}
                refElementData={refData}
                scheduleStart={scheduleStart}
                scheduleLength={scheduleLength}
                meeting={meeting}/>
        })}
        <Spotter refElementData={refData} scheduleStart={scheduleStart} scheduleLength={scheduleLength}/>
    </div>
    </>
}