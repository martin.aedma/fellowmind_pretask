import React, { useState } from 'react'
import {Meeting} from '../../@types/custom'
import time from '../../icons/time.svg'
import description from '../../icons/description.svg'
import person from '../../icons/person.svg'
import up from '../../icons/up.svg'
import {getHoursAndMinutes} from '../../utilities/functions'

interface EventViewProps {
    setIsViewingEvent: (value: ViewingEvent) => void;
    isViewingEvent: ViewingEvent
}
interface ViewingEvent {
    isViewing: boolean;
    meeting: Meeting
}

const Days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
]

export const EventView = ({isViewingEvent, setIsViewingEvent}: EventViewProps):JSX.Element => {

    const [showParticipants, setShowParticipants] = useState(false)

    const toggleEventView = () => {
        return (event: React.MouseEvent) => {
            setIsViewingEvent({isViewing: false, meeting: isViewingEvent.meeting})
        }
    }

    const displayDate = (date: Date):string => {
        return `${Days[date.getDay()].toUpperCase()}, ${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()}`
    }

    const toggleParticipants = () => {
        return (event: React.MouseEvent) => {
            if(isViewingEvent.meeting.Participants && isViewingEvent.meeting.Participants.length > 0){
                setShowParticipants(prev => !prev)
            }
        }
    }

    return <>
        <div className="sidebar-header-container event-view-container" onClick={toggleEventView()}>
            <span className='event-view-return'>&lt;</span>
            <span className='event-view-header'>{isViewingEvent.meeting.Subject}</span>
        </div>
        <div className='sidebar-schedule-container no-padding'>
            <div className='sidebar-header-container event-view-info'>
                <img alt='time icon' src={time}/>
                <span>{ displayDate(new Date(isViewingEvent.meeting.StartTime))}</span>
            </div>
            <div className='sidebar-header-container event-view-info'>
                <img alt="time icon" src={time}/>
                <span>{ getHoursAndMinutes(new Date(isViewingEvent.meeting.StartTime))} TO { getHoursAndMinutes(new Date(isViewingEvent.meeting.EndTime))}</span>
            </div>
            <div
                className='sidebar-header-container event-view-info'
                onClick={toggleParticipants()}
                style={{cursor: 'pointer'}}>
                <img alt='person icon' src={person}/>
                <span>PARTICIPANTS</span>
                { isViewingEvent.meeting.Participants && isViewingEvent.meeting.Participants.length > 0 &&
                    <img alt='arrowhead' src={up} className={`arrowhead ${showParticipants ? 'rotate-down' : ''}`}/>}
            </div>
            <div className={`sidebar-header-container event-view-info participants ${showParticipants ? 'show-participants' : ''}`}>
                {isViewingEvent.meeting.Participants?.map((participant, index) => {
                    return <div key={index} className='participant-container'>
                        <div className='participant-stub'></div>
                        <div className='participant-circle'></div>
                        <div className='participant-info-container'>
                            <span className='participant-name'>{participant.Name}</span>
                            <span className='participant-title'>{participant.Title}</span>
                        </div>
                    </div>
                })}
            </div>
            <div className='sidebar-header-container event-view-info'>
                <img alt='description icon' src={description}/>
                <span>DESCRIPTION</span>
            </div>
            <div className='sidebar-header-container event-view-info'>
                <div className='participant-stub'></div>
                <span className='participant-title' style={{textTransform:'none'}}>"{isViewingEvent.meeting.Subject}"</span>
            </div>
        </div>
    </>
}