import { useState, useEffect } from 'react'

interface SpotterProps {
    refElementData: RefElementData;
    scheduleStart: number;
    scheduleLength: number;
}

interface RefElementData {
    scheduleContainerHeight: number;
    timelineContainerHeigth: number;
    timelineContainerWidth: number;
}

export const Spotter = ({refElementData, scheduleStart, scheduleLength}:SpotterProps):JSX.Element => {
    const [offsetY, setOffsetY] = useState(0)

    const updateSpotter = () => {
        setOffsetY( prev => prev + calculateOffsetY(refElementData.scheduleContainerHeight) - prev)
    }

    useEffect(() => {
        const interval = setInterval(updateSpotter, 1000);
        return () => clearInterval(interval);
    }, [refElementData])

    const calculateOffsetY = (containerHeigth: number):number => {
        const today = new Date()
        const timeNow = today.getHours() + today.getMinutes()/60
        const normalize = timeNow - scheduleStart
        const percentageFromSchedule = normalize / scheduleLength
        return Math.round(percentageFromSchedule * containerHeigth + scheduleLength - normalize * normalize / 1.01 )
    }

    return <div className="sidebar-spotter-container" style={{height: `${refElementData.timelineContainerHeigth}px`, transform: `translateX(35px)`, top: offsetY}}>
        <span className="sidebar-spotter-circle"></span>
        <hr className="sidebar-spotter-line"/>
    </div>
}