
import React from "react";
import { RefObject } from "react"
import { useEffect, useState, useRef } from "react"

interface ResizeRect {
    width: number;
    heigth: number;
}

interface Breakpoints {
  one: number;
  two: number;
}

export const getHoursAndMinutes = (date: Date):string =>{
    return `${date.getHours()}:${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes() }`
}

export const useResponsiveBreakpoints = (elRef: RefObject<HTMLDivElement>):ResizeRect | undefined => {
    const [breakSizes, setBreakSizes] = useState<ResizeRect>()

    const observer = useRef(
      new ResizeObserver(entries => {
        const { width } = entries[0].contentRect
        const { height } = entries[0].contentRect
        setBreakSizes({width: width, heigth: height})
      })
    )

    useEffect(() => {
      let elem: RefObject<HTMLDivElement>;
      let obv: React.MutableRefObject<ResizeObserver>

      if (elRef.current) {
        elem = elRef
        obv = observer
        observer.current.observe(elRef.current)
      }

      return () => {
          if(elem.current){
            obv.current.unobserve(elem.current)
          }
      }
    }, [elRef, observer])

    return breakSizes
  }

  export const findBreakPoint = (breakpoints: Breakpoints, width: number): number => {

    if(width <= breakpoints.one) {
      return 1
    }

    if (width > breakpoints.one && width <= breakpoints.two) {
      return 2
    }

    return 3
  }