export interface Meeting {
    Subject: string;
    Organizer: string;
    StartTime: string;
    EndTime: string;
    Participants: Participant[] | null;
}

export interface Participant {
    Name: string;
    Title: string;
}