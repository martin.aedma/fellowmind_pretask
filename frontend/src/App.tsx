import * as React from 'react';
import { useEffect, useState, useRef } from 'react';
import { MainView } from './components/MainView/MainView';
import { SideBar } from './components/SideBar/SideBar';
import './styles/index.scss';
import { useResponsiveBreakpoints } from './utilities/functions';
import timetable_white from './icons/timetable_white.svg'
import timetable_black from './icons/timetable_black.svg'

interface Meeting {
  Subject: string;
  Organizer: string;
  StartTime: string;
  EndTime: string;
  Participants: Participant[] | null;
}

interface Participant {
  Name: string;
  Title: string;
}

const App = ():JSX.Element => {

  const [currentMeeting, setCurrentMeeting] = useState<Meeting | false>(false)
  const [showMobile, setShowMobile] = useState(false)
  const [isViewingMain, setIsViewingMain] = useState(true)
  const containerRef = useRef<HTMLDivElement>(null)
  const sizes = useResponsiveBreakpoints(containerRef)
  const [date, setDate] = useState(new Date())

  useEffect(() => {
    if(sizes?.width){
      if(sizes.width <700){
          setShowMobile(true)
      }
      if(sizes.width >= 700){
          setShowMobile(false)
      }
  }
  }, [sizes])

  useEffect(() => {
     setCurrentMeeting(checkCurrentMeeting())
  }, [date])

  useEffect(() => {
    const interval = setInterval(() => {
      setDate(prev => new Date())
    }, 60000);
    return () => clearInterval(interval);
  }, [])

  const handleClick = () => {
    return (event: React.MouseEvent) => {
      setIsViewingMain(prev => !prev)
    }
  }

  const checkCurrentMeeting = ():Meeting | false => {
    const currentTime = new Date().getTime();
    // check meetings for current time
    for(const meeting of meetings){
      const startTime = new Date(meeting.StartTime).getTime();
      const endTime = new Date(meeting.EndTime).getTime();
      if (currentTime >= startTime && currentTime <= endTime){
        return meeting
      }
    }
    return false
  }

  return <div className="app-container" ref={containerRef}>
      <MainView
        currentMeeting={currentMeeting}
        meetings={meetings}
        isViewingMain={isViewingMain}
        showMobile={showMobile}/>

      <SideBar
        isViewingMain={isViewingMain}
        showMobile={showMobile}
        meetings={meetings}/>

      <div className={`mobile-view-toggle ${showMobile ? 'show-mobile-toggle' : ''}`}>
        <img alt='toggle mobile view' src={ isViewingMain ? timetable_white : timetable_black} onClick={handleClick()}/>
      </div>
    </div>
}

const meetings:Meeting[] = [
  {
    "Subject": "eCraft Management Monthly Meeting",
    "Organizer": "Ville Hemmilä",
    "StartTime": "2022-02-04T08:00:00",
    "EndTime": "2022-02-04T16:30:00",
    "Participants": [
      {
        Name: "Juho Röyhy",
        Title: "Art Director"
      },
      {
        Name: "Petteri Lehtonen",
        Title: "UX Architect"
      },
      {
        Name: "Tero Tapanainen",
        Title: "CTO"
      }
    ]
  },
  {
    "Subject": "Jooses Design workshop",
    "Organizer": "Joose Rautemaa",
    "StartTime": "2022-02-04T17:00:00",
    "EndTime": "2022-02-04T18:00:00",
    "Participants": null
  },
  {
    "Subject": "Joose goes sickbr0",
    "Organizer": "Joose Rautemaa",
    "StartTime": "2022-02-04T18:15:00",
    "EndTime": "2022-02-04T19:30:00",
    "Participants": null
  },
  {
    "Subject": "Nousiainen Inspection",
    "Organizer": "Joose Rautemaa",
    "StartTime": "2022-02-03T20:00:00",
    "EndTime": "2022-02-03T21:30:00",
    "Participants": null
  }
]


export default App;
