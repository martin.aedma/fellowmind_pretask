# fellowmind_pretask

## FrontEnd

Frontend is developed with React (Create React App) and TypeScript.
App is responsive and has mobile view with a button to toggle between main view and sidebar.

![mobile views](https://gitlab.com/martin.aedma/fellowmind_pretask/-/raw/main/first.png)

To run, npm install inside frontend folder and npm start to launch.

App is also available online on 3-4.Feb.2022 at http://178.128.137.110:8080/

When no meetings are currently underway, clock is displayed instead on main view.
Meetings are displayed when they are happening time wise currently. There were 4 meetings given to use in this assignment but dates they had were a few years ago. I updated start and end times of those events to 03 / 04 / 2022 so they will be displayed on that day. If you want to see events on other times, meeting timestamps have to be modified.

### Issue on FrontEnd

Spotter is the yellow line which should correspond to current time. Spotter moves from top of schedule view (earliest time) to bottom (towards evening). They way I implemented this is to get Schedule container element size and map it to Schedule length. If Schedule starts 7:00 that is at pixel 0, at schdedule end (22:30 currently) will be the last pixel height. Spotter moves corresponding to current time and is updated every second.
Idea behind this implementation should be ok, however there seems to be something unaccounted for because there is a error. Spotter is a off a little at the center of schedule, but more obviously at start of schedule and at the end schedule.

## BackEnd

I went on and tried to implement Backend with C# / ASP .NET CORE 6 and use the new Minimal API. I added Postgres SQL database with 3 tables:

* Meetings
* Participants
* Meetings_Participants

I added endpoints to add data. Adding data to each of them works. At the very end I ran into a problem I could not solve, that is to get data of events happening on current_day with participants in them.

I got SQL query to work when executed directly from Postgres Shell like so

![Postgres Shell query](https://gitlab.com/martin.aedma/fellowmind_pretask/-/raw/main/second.png)

BUT, when I took it to Entity Framework query always returned participants list as Null ?!?

Query in Entity Framework
![Entity Framework](https://gitlab.com/martin.aedma/fellowmind_pretask/-/raw/main/third.png)

Result in Swagger
![Swagger](https://gitlab.com/martin.aedma/fellowmind_pretask/-/raw/main/fourth.png)

My knowledge in Entity Framework was not enough, I did learn that a better approach for raw queries wouldve been to use Dapper. I definitely want to lear to use .NET CORE for backend.
